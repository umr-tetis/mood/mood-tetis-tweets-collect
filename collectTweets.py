#!/usr/bin/env python

"""
@brief Collect Tweets
@author Remy Decoupes
@copyright CeCILL-B

Connect to Twitter stream using Twitter API and filter tweets which have to be retrieved with
    - Account to follow : accountsFollowed.csv
    - Hashtag to follow : keywordsFilter.csv

To install and run this script : please follow instructions from README.md
"""
import tweepy
import sys
import logging
from logging.handlers import RotatingFileHandler
import pandas as pd
import time


def exitscript(logger, message):
    """
    Log error and exit script
    :param logger: a logger object
    :param message: print a message
    :return:
    """
    logger.error("The program encountered an error")
    logger.error(msg)
    logger.error("End of execution.")
    sys.exit(1)


def logsetup():
    """
    Initiate a logger object :
        - Log in file : collectweets.log
        - also print on screen
    :return: logger object
    """
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    file_handler = RotatingFileHandler('log/collectweets.log', 'a', 1000000, 1)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    stream_handler = logging.StreamHandler()
    # Only display on screen INFO
    stream_handler.setLevel(logging.INFO)
    logger.addHandler(stream_handler)
    return logger


class Listener(tweepy.StreamListener):
    def __init__(self, output_file=sys.stdout, logger=sys.stdout, keywords=[]):
        super(Listener, self).__init__()
        self.output_file = output_file
        self.logger = logger
        self.logger.info("initiate stream listener")
        self.keywords = keywords

    def on_status(self, status):
        """
        Here a list of attributs/properties of status object :
        ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__',
        '__getattribute__', '__getstate__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__',
        '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__',
        '__str__', '__subclasshook__', '__weakref__', '_api', '_json', 'author', 'contributors', 'coordinates',
        'created_at', 'destroy', 'entities', 'favorite', 'favorite_count', 'favorited', 'filter_level', 'geo', 'id',
        'id_str', 'in_reply_to_screen_name', 'in_reply_to_status_id', 'in_reply_to_status_id_str','in_reply_to_user_id',
         'in_reply_to_user_id_str', 'is_quote_status', 'lang', 'parse', 'parse_list', 'place', 'possibly_sensitive',
         'quote_count', 'reply_count', 'retweet', 'retweet_count', 'retweeted', 'retweeted_status', 'retweets','source',
          'source_url', 'text', 'timestamp_ms', 'truncated', 'user']

        More information could be find on Tweepy's github :
        https://github.com/tweepy/tweepy/blob/master/tweepy/streaming.py

        :param status:
        :return:
        """
        try:
            # Get tweets only is they match with a list of keywords (on a plain-text and on hasthag)
            if any(keyword in str(status._json) for keyword in self.keywords):
                print(status._json, file=self.output_file)
            # print status object properties
            # print(dir(status))
        except:
            msg = "can not save tweets in file"+str(self.output_file)
            exitscript(logger, msg)

    def on_error(self, status_code):
        logger.error("Error on stream twitter: "+str(status_code))
        return False

    def on_limit(self, track):
        """ Log if Twitter stream limitation is reached, i.e. our collect > 1% Firehorse of Twitter"""
        logger.error("Twitter limit reached: "+str(track))


if __name__ == '__main__':
    """
    Create a jsonl file which contains json lines of tweets.
    This script filter Twitter stream by a list of account followed and then only keeps tweets which match with
    a list of words.
    Both account and list of keywords are defined by MOOD project.
    """
    # initialize a logger :
    logger = logsetup()
    logger.info("Collect tweets : start")

    # try import credentials of MOODTwitter account
    try:
        from params import credentials
    except ImportError:
        msg = 'it seems there is no file named :"credentials.py"'
        exitscript(logger, msg)

    # Access and authorize on MOOD twitter Account
    try:
        auth = tweepy.OAuthHandler(credentials.consumer_key, credentials.consumer_secret)
        auth.set_access_token(credentials.access_token, credentials.access_token_secret)
        api = tweepy.API(auth)
        # Get the User object for twitter...
        accountused = api.me()
        logger.info("Log with: " + accountused.name)
    except tweepy.TweepError as twe:
        msg = "Wrong credentials: please check credentials.py"
        exitscript(logger, msg)
    except Exception as e:
        msg = "Please double check credentials.py :" + e
        exitscript(logger, msg)

    # Get twitter ID of account
    accounttofollowed = pd.read_csv("params/accountsFollowed.csv")
    accounttofollowedlist = list(map(str,accounttofollowed['twitterID'].tolist()))

    # Get keyword to track
    keyword = pd.read_csv("params/keywordsFilter.csv")
    keywordList = list(map(str, keyword['hashtags'].tolist()))

    # Start a Twitter stream
    timestr = time.strftime("%Y%m%d-%H%M%S")
    tweetouputfilename = "output/tweetoutput"+timestr+".jsonl"
    tweetoutput = open(tweetouputfilename, 'w')
    myStreamListener = Listener(tweetoutput, logger, keywordList)
    stream = tweepy.Stream(auth=api.auth, listener=myStreamListener)
    while True:
        try:
            logger.info("Start streaming")
            # Could not filter on both ("AND") account AND hashtag because Twitter's API query only implements "OR"
            # stream.filter(follow=accounttofollowedlist, track=hashtagtrackedList)
            stream.filter(follow=accounttofollowedlist)
        except KeyboardInterrupt:
            logger.info("Stream Keyboard Interrupt")
            logger.info("Collect tweets : proceeded normally")
            stream.disconnect()
            tweetoutput.close()
            sys.exit(1)
        except Exception as ex:
            # Example of errors caught in dev : ReadTimeoutError / ProtocolError
            logger.error("Unexpected error with stream. Try reconnect")
            logger.error("Error: "+str(ex))
            logger.error("error.args: ", str(ex.args))
            # Avoid Twitter error 420 : too many login. Wait for 10 seconds
            time.sleep(10)
            continue
            # logger.info("Collect tweets : aborted")

