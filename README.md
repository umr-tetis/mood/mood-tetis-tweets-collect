# Collect tweets for H2020 MOOD

This project collects tweets based on a list of keywords or/and twitter accounts in the [H2020 MOOD](https://mood-h2020.eu/) framework

Keywords and Twitter accounts lists have been defined by H2020 MOOD partners :
* [keyword](params/keywordsFilter.csv)
* [twitter account](params/accountsFollowed.csv). For MOOD partners, the selection of accounts to follow can be found in [Alfresco](https://collaboratif.cirad.fr/share/page/site/MOODProposal/document-details?nodeRef=workspace://SpacesStore/de5d3d86-356e-4383-9078-cf4b9606c76a)

**To be compliant with Twitter's [Terms of Service](https://developer.twitter.com/en/developer-terms/agreement-and-policy)** this repository doesn't contains any tweet nor Twitter credentials. 
Users who wants to use this project have to create a [Twitter developer account](https://developer.twitter.com/en)

## Installation
### Dependencies
This project has only been tested on python3.7. 
Install all dependencies using pip:
```shell
pip3 install -r requirements.txt
```
### Twitter credentials
A credentials.py has to be created using the [template](params/template-credentials.py)
1. First copy the template :
    ```shell
    cp params/template-credentials.py params/credentials.py 
    ```
2. Edit credentials.py with your own Twitter account credentials
## Run
The run of this script will create a log file.
The script can be launched using this command :
```shell
python3 collectTweets.py
```
## Optional: create a systemd deamon
1. Copy service file `sudo cp systemd/mood-tweets-collect.service /lib/systemd/system`
2. Modify `/lib/systemd/system/mood-tweets-collect.service with the correct `WorkingDirectory` and `User
3. Reload systemd :`sudo systemctl daemon-reload`
4. Enable auto start using command : `sudo systemctl enable mood-tweets-collect.service`
## Optional analysis
### Index tweets and explore data with ELK stack:
<img src="https://images.contentstack.io/v3/assets/bltefdd0b53724fa2ce/blt280217a63b82a734/5bbdaacf63ed239936a7dd56/elastic-logo.svg" alt="elastic" width="200">

See [documentation](elasticsearch/indexation.md)
### Visualize Retweet mechanism thank to gephi
<img src="https://gephi.org/images/logo.png" alt="gephi">

1. Install gephi with snap: `snap install gephi`
2. Export 2 files :
   1. Edge with `["ID", "Source", "Target", "timeset"]`. Be careful name of columns are very important otherwise Gephi won't recognize it. Timeset is not mandatory: use it if you want dynamic graph
   2. Nodes with `["ID", Label"]`
3. Import both files with gephi
4. Change the spatial layout (force atlas 2 for example)
5. Change size of nodes corresponding to their importance (degree in and out)
6. Change color of nodes according to their community:
   1. Apply a community detection algo: we use modularity (from gephi statisic: cluster nodes when they share a lot of link and less much with nodes out of their communauty)
   2. On Aspect, choose partition and select the modularity class that they have just been created

## License
This code is provided under the [CeCILL-B](https://cecill.info/licences/Licence_CeCILL-B_V1-en.html) free software license agreement.
