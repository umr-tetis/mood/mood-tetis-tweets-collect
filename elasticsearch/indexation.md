# Indexation of tweets with Elasticsearch

## Installation
1. Install ELK (version used by authors: 7.x): logstash, elasticsearch and kibana: [elk-documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html)
    + Prepare apt source list:
        + `wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -`
	+ `sudo apt-get install apt-transport-https`
	+ `echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
`
	+ `sudo apt-get update`
    + Install ELK packages:
        + `sudo apt install elasticsearch`
        + `sudo apt install logstash`
        + `sudo apt install kibana`
2. Configure elasticsearch
    + Change elastic defaut directory data and log to the current tree:
        + edit /etc/elasticsearch/elasticsearch.yml
        + 
		```
		# path.data: /var/lib/elasticsearch
		path.data: [absolute_path]/mood-tetis-tweets-collect/elasticsearch/data
		#
		# Path to log files:
		#
		# path.logs: /var/log/elasticsearch
		path.logs: [absolute_path]/mood-tetis-tweets-collect/elasticsearch/log
		```
    + Change ownership of these directories:
	```
	sudo chown elasticsearch [absolute_path]/mood-tetis-tweets-collect/elasticsearch/data
	sudo chown elasticsearch [absolute_path]/mood-tetis-tweets-collect/elasticsearch/log 
	```
3. Configure kibana to bind on other iface than localhost:
    + edit /etc/kibana/kibana.yml
    + `server.host: "mo-mood-tetis-tweets-collect"`
3. Install a plugin for logstash to geocode user location (plugin is for using API Rest):
    `sudo /usr/share/logstash/bin/logstash-plugin install logstash-filter-rest`
4. Configure logstash
	+ Create a simulink for logstash configuration: `sudo ln -s [absolute_path]/mood-tetis-tweets-collect/elasticsearch/logstash/mood-tetis-tweets-collect.conf /etc/logstash/conf.d/`
	+ Change logstash's log directory to your working directory (pay attention to change owner to the user "logstash":
		+ `sudo vim /etc/logstash/logstash.yml` Search path.logs and change the directory
		+ `sudo chown logstash [logstash's log dir]`
		+ `sudo ln -s /home/rdecoupe/mood-tetis-tweets-collect/elasticsearch/log/logstash /var/log/logstash/`: create a simulink to the default system log directory (/var/log)
	+ Configure logstash to start indexation from where it previously stopped: 
		+ `sudo touch [absolute_path]/elasticsearch/logstash/sincedb.log`
		+ change ownership to logstash user: `sudo chown logstash sincedb.log`
## Start
1. Start Elastic and kibana
	+ `sudo systemctl start elasticsearch.service`
	+ `sudo systemctl start kibana.service`
2. Verify if kibana is working (and is connected to elasticsearch)
	+ With your favorite browser connect to [Mood tetis kibana website](http://mo-mood-tetis-tweets-collect.montpellier.irstea.priv:5601/app/home#/)
	+ Create an index pattern through Analytics > Discover
3. Edit tweets jsonl file: transform the single quote to double quote: `python3 elasticsearch/src/fix_bad_quote_json.py`
